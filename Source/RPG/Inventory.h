// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#pragma once

#include "InventoryHelpers.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Inventory.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class RPG_API UInventory : public UObject
{
	GENERATED_BODY()

public:
	//Constructor
	UInventory(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	int32 Gold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<FInventoryItem> Items;

public:

	UFUNCTION(BlueprintCallable)
	bool Add(FInventoryItem item);

	UFUNCTION(BlueprintCallable)
	bool Remove(FInventoryItem item);

	UFUNCTION(BlueprintCallable)
	bool Contains(FInventoryItem item);

	UFUNCTION(BlueprintCallable)
	int32 Find(FInventoryItem item);
};
