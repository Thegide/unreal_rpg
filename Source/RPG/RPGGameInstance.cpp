// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#include "RPGGameInstance.h"

URPGGameInstance::URPGGameInstance(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{

}

void URPGGameInstance::Init()
{

	Inventory = CreateNewInventory();

	//Initialize starter inventory
	if (Inventory) 
	{
		
		//Add some starter gold to inventory
		Inventory->Gold = 150;

		//Add a few basic items to inventory
		FInventoryItem Potion = UInventoryHelpers::GetItemByRow(FName(TEXT("0")));
		FInventoryItem Revive = UInventoryHelpers::GetItemByRow(FName(TEXT("6")));
			
		Potion.Quantity = 3; //not working
		Inventory->Add(Potion);
		Inventory->Add(Revive);

		//Add a sword

		//Add clothes
	}
}


UInventory* URPGGameInstance::CreateNewInventory()
{
	UInventory* inv = NewObject<UInventory>();

	return inv;
}

//UInventory * URPGGameInstance::GetInventory()
//{
//	return Inventory;
//}
