// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#include "Inventory.h"

UInventory::UInventory(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
}

bool UInventory::Add(FInventoryItem item)
{
	if (!Items.Contains(item))
	{
		UE_LOG(LogTemp, Warning, TEXT("%s added to inventory"), *item.Name.ToString());

		Items.Add(item);
		return true;
	}

	return false;
}

bool UInventory::Remove(FInventoryItem item)
{
	if (Items.Contains(item))
	{
		UE_LOG(LogTemp, Warning, TEXT("%s removed from inventory"), *item.Name.ToString());

		Items.Remove(item);
		return true;
	}

	return false;
}

bool UInventory::Contains(FInventoryItem item)
{
	UE_LOG(LogTemp, Warning, TEXT("%s found in inventory"), *item.Name.ToString());

	return Items.Contains(item);
}

int32 UInventory::Find(FInventoryItem item)
{
	return Items.Find(item);
}
