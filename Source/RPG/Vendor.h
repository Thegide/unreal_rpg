// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#pragma once

#include "Inventory.h"
#include "RPGGameInstance.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Vendor.generated.h"


UCLASS()
class RPG_API AVendor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVendor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vendor Data")
	FName Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Vendor Data")
	UInventory* ShopInventory;

protected:
	
	URPGGameInstance* GameInstance;
	UInventory* PlayerInventory;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void BuildInventoryFromDatabase();
		

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool Buy(FInventoryItem Item);

	bool Sell(FInventoryItem Item);

};
