// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#include "InventoryHelpers.h"

UInventoryHelpers::UInventoryHelpers(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer)
{
	//Load ItemData data table
	static::ConstructorHelpers::FObjectFinder<UDataTable> ItemTable(TEXT("/Game/Data/ItemData"));
	if (ItemTable.Object)
	{
		ItemLookupTable = ItemTable.Object;
		UE_LOG(LogTemp, Warning, TEXT("ItemData load successful"));
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("Unable to load ItemData"));

}

//what if rowname is not valid?
FInventoryItem UInventoryHelpers::GetItemByRow(FName RowName)
{
	const TArray<FName>& RowNames = ItemLookupTable->GetRowNames();
	FItemLookupRow* RowItem = ItemLookupTable->FindRow<FItemLookupRow>(RowName, ContextString);
	
	if (!RowItem)
	{
		UE_LOG(LogTemp, Warning, TEXT("Row %s not found in item database"), *RowName.ToString());
	}

	return ConvertInventoryItemRow(*RowItem);
}

//what if itemname is not valid?
FInventoryItem UInventoryHelpers::GetItemByName(FText Name)
{
	const TArray<FName>& RowNames = ItemLookupTable->GetRowNames();
	int32 index = 0;
	FItemLookupRow* RowItem;
	FItemLookupRow* FoundItem = nullptr;

	for (auto& name : RowNames)
	{
		RowItem = ItemLookupTable->FindRow<FItemLookupRow>(name, ContextString);
		if (RowItem && (RowItem->Name.EqualTo(Name))) 
		{
			FoundItem = RowItem;
			break;
		}
	}

	if (!FoundItem)
	{
		UE_LOG(LogTemp, Warning, TEXT("Item %s not found in item database"), *Name.ToString());
	}

	return ConvertInventoryItemRow(*FoundItem);	
}

FInventoryItem UInventoryHelpers::ConvertInventoryItemRow(const FItemLookupRow & Row)
{
	FInventoryItem Item;

	Item.ItemID = Row.ItemID;
	Item.Name = Row.Name;
	Item.Thumbnail = Row.Thumbnail;
	Item.Description = Row.Description;
	Item.Value = Row.Value;
	Item.isUnique = Row.isUnique;
	Item.isStackable = Row.isStackable;
	Item.consumeOnUse = Row.consumeOnUse;
	Item.Quantity = 1;

	return Item;
}
