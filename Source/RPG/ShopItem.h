// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ShopItem.generated.h"

USTRUCT(BlueprintType)
struct FShopItem : public FTableRowBase
{
	GENERATED_BODY()
	
public:

	FShopItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shop Data")
	FName VendorName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shop Data")
	FName ItemID;

	//To be used if item sell values fluctuate depending on location
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shop Data")
	//int32 Cost;
};
