// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#pragma once

#include "Inventory.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RPGGameInstance.generated.h"

UCLASS()
class RPG_API URPGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	URPGGameInstance(const FObjectInitializer & ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UInventory* Inventory;
	
	//currently not needed as Inventory is public
	//UFUNCTION(BlueprintCallable)
	//UInventory* GetInventory();

	virtual void Init() override;

protected:

	UInventory* CreateNewInventory();
};
