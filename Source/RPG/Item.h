// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Inventory.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

UCLASS()
class RPG_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	UPROPERTY(EditAnywhere, Category = "Item")
	FInventoryItem ItemData;

	UPROPERTY(EditAnywhere, Category = "Item")
	UStaticMesh* Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
