// (c) 2018 Maelstrom Studios.  All Rights Reserved.

#pragma once

#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InventoryHelpers.generated.h"

/** Structure to store the lookup of Inventory objects for use in a UDataTable */
 USTRUCT(Blueprintable)
struct FItemLookupRow : public FTableRowBase
{
	GENERATED_BODY()

public:

	// ItemID to be used for lookups
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	int32 ItemID;

	// Name of the item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	FText Name;

	// Cost of the item (purchase price)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	int32 Cost;

	// Sale value of the item (sell price)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	int32 Value;

	// UI Image
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	UTexture2D* Thumbnail = nullptr;

	// Description of the item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	FText Description;

	// If the item can be stacked, how may items can be in a stack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	int32 MaxStackSize;

	// Whether the item is unique (limit 1 per inventory) 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	bool isUnique;

	// Whether the item can be stacked
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	bool isStackable;

	// Should the item be consumed on use
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	bool consumeOnUse;
};

 /** Structure to inventory items */
USTRUCT(BlueprintType)
struct FInventoryItem : public FTableRowBase
 {
	 GENERATED_BODY()

 public:

	 // ItemID to be used for lookups
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 int32 ItemID;

	 // Name of the item
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 FText Name;

	 // UI Image
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 UTexture2D* Thumbnail = nullptr;

	 // Description of the item
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 FText Description;

	 // Sale value of the item (sell price)
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 int32 Value;

	 // The number of items currently held
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 int32 Quantity;

	 // Whether the item is unique (limit 1 per inventory) 
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 bool isUnique;

	 // Whether the item can be stacked
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 bool isStackable;

	 // Should the item be consumed on use
	 UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Data")
	 bool consumeOnUse;

	 bool operator==(const FInventoryItem& OtherItem) const
	 {
		 if (ItemID == OtherItem.ItemID)
			 return true;
		 return false;
	 }
 };

// Data table for all item data
static  UDataTable* ItemLookupTable;

// Needed for lookup calls
static const FString ContextString(TEXT("GENERAL"));

UCLASS()
class RPG_API UInventoryHelpers : public UObject
{
	GENERATED_BODY()
	
public:

	UInventoryHelpers(const FObjectInitializer & ObjectInitializer);

	static FInventoryItem GetItemByRow(FName RowName);

	static FInventoryItem GetItemByName(FText Name);

	static FInventoryItem ConvertInventoryItemRow(const FItemLookupRow& Row);

};
